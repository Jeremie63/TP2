function clic()
{
    var champ = document.getElementById("champ").value;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            var arr = JSON.parse(this.responseText);
            for (var i = 0; i < arr.length; i++)
            {
                var iDiv = document.createElement('div');
                var obj = arr[i];

                iDiv.id = 'res';
                iDiv.className = 'block';
                document.getElementsByTagName('form')[0].appendChild(iDiv);

                if (i==0)
                {
                    iDiv.innerHTML += "<br><br>Résultats pour : " + obj;
                }
                
                else if (i==1)
                {
                    for (var key in obj)
                    {
                        var value = obj[key];
                        var numero = parseInt(key)+1;
                        iDiv.innerHTML += "<br> - " + "Résultat n°" + numero + ": " + value + "<br>" + arr[2][i-1] + "<br><a href='" + arr[3][i-1] + "'>" + arr[3][i-1] + "</a>" + "<br>";
                        i++;
                    }
                }
            }
        }
    };
    xhttp.open("GET", "http://localhost:1234/api?action=opensearch&search="+champ, true);
    xhttp.send();
}